
# EASY

# Define a method that, given a sentence, returns a hash of each of the words as
# keys with their lengths as values. Assume the argument lacks punctuation.
def word_lengths(str)
  word_length_hash = {}
  str.split.each do |word|
    word_length_hash[word] = word.length
  end
  word_length_hash
end

# Define a method that, given a hash with integers as values, returns the key
# with the largest value.
def greatest_key_by_val(hash)
  hash.sort_by { |_key, value| value }[-1].first
end

# Define a method that accepts two hashes as arguments: an older inventory and a
# newer one. The method should update keys in the older inventory with values
# from the newer one as well as add new key-value pairs to the older inventory.
# The method should return the older inventory as a result. march = {rubies: 10,
# emeralds: 14, diamonds: 2} april = {emeralds: 27, moonstones: 5}
# update_inventory(march, april) => {rubies: 10, emeralds: 27, diamonds: 2,
# moonstones: 5}
def update_inventory(older, newer)
  newer.each do |key, val|
    older[key] = val
  end
  older
end

# Define a method that, given a word, returns a hash with the letters in the
# word as keys and the frequencies of the letters as values.
def letter_counts(word)
  letter_counts = Hash.new(0)
  word.chars.each do |ch|
    letter_counts[ch] += 1
  end
  letter_counts
end

# MEDIUM

# Define a method that, given an array, returns that array without duplicates.
# Use a hash! Don't use the uniq method.
def my_uniq(arr)
  hash = {}
  arr.each do |key|
    hash[key] = 'val'
  end
  hash.keys
end

# Define a method that, given an array of numbers, returns a hash with "even"
# and "odd" as keys and the frequency of each parity as values.
def evens_and_odds(numbers)
  even_odd_count = Hash.new(0)
  numbers.each do |num|
    if num.even?
      even_odd_count[:even] += 1
    else
      even_odd_count[:odd] += 1
    end
  end
  even_odd_count
end

# Define a method that, given a string, returns the most common vowel. If
# there's a tie, return the vowel that occurs earlier in the alphabet. Assume
# all letters are lower case.
def most_common_vowel(string)
  vowel_frequency = Hash.new(0)
  string.chars.each do |ch|
    if 'aeiou'.include?(ch)
      vowel_frequency[ch] += 1
    end
  end
  vowel_frequency.select! do |_key, val|
    val == vowel_frequency.values.max
  end
  vowel_frequency.sort[0].first
end

# HARD

# Define a method that, given a hash with keys as student names and values as
# their birthday months (numerically, e.g., 1 corresponds to January), returns
# every combination of students whose birthdays fall in the second half of the
# year (months 7-12). students_with_birthdays = { "Asher" => 6, "Bertie" => 11,
# "Dottie" => 8, "Warren" => 9 }
# fall_and_winter_birthdays(students_with_birthdays) => [ ["Bertie", "Dottie"],
# ["Bertie", "Warren"], ["Dottie", "Warren"] ]
def fall_and_winter_birthdays(students)
  combinations = []
  students = students.select! { |_, birthday| birthday > 6 }.keys
  students.each_with_index do |student, i|
    other_students = students[i + 1..-1]
    other_students.each do |fellow_student|
      combinations << [student, fellow_student]
    end
  end
  combinations
end

# Define a method that, given an array of specimens, returns the biodiversity
# index as defined by the following formula: number_of_species**2 *
# smallest_population_size / largest_population_size biodiversity_index(["cat",
# "cat", "cat"]) => 1 biodiversity_index(["cat", "leopard-spotted ferret",
# "dog"]) => 9
def biodiversity_index(specimens)
  index = Hash.new(0)
  specimens.each do |specimen|
    index[specimen] += 1
  end
  index.length**2 * index.values.min / index.values.max
end

# Define a method that, given the string of a respectable business sign, returns
# a boolean indicating whether pranksters can make a given vandalized string
# using the available letters. Ignore capitalization and punctuation.
# can_tweak_sign("We're having a yellow ferret sale for a good cause over at the
# pet shop!", "Leopard ferrets forever yo") => true
def can_tweak_sign?(normal_sign, vandalized_sign)
  count_of_vandal = character_count(vandalized_sign)
  count_of_normal = character_count(normal_sign)
  count_of_vandal.each do |ch, count|
    if count_of_normal[ch] < count
      return false
    end
  end
  true
end

def character_count(str)
  count = Hash.new(0)
  str.downcase.chars.each do |ch|
    count[ch] += 1
  end
  count
end
